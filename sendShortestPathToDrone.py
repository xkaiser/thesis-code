#import socket module
#Dijkstra's algorithm python implementation adapted from 
#https://gist.github.com/mdsrosa/c71339cb23bc51e711d8

#Finds the shortest path from the home location to the target location following
#the hard coded waypoints. Sends this path to the drone so that it will start
#the mission. This sends the path to the drone, and then back home.
import sys, math
from socket import *
from collections import defaultdict, deque

#Argv defines
COORDFILE_INDEX = 1
EDGEFILE_INDEX = 2
ARGV_LAT_INDEX = 3
ARGV_LONG_INDEX = 4

#Index defines (used in algorithm)
LAT_INDEX = 1
LONG_INDEX = 2

class Graph(object):
	def __init__(self):
		self.nodes = set()
		self.edges = defaultdict(list)
		self.distances = {}

	def add_node(self, value):
		self.nodes.add(value[0])

	def add_edge(self, from_node, to_node):
		self.edges[from_node[0]].append(to_node[0])
		self.edges[to_node[0]].append(from_node[0])
		self.distances[(from_node[0], to_node[0])] = get_distance(from_node, to_node)

def get_distance(p0, p1):
	return math.sqrt((p0[LAT_INDEX] - p1[LAT_INDEX])**2 + (p0[LONG_INDEX] - p1[LONG_INDEX])**2)

def dijkstra(graph, initial):
    visited = {initial: 0}
    path = {}

    nodes = set(graph.nodes)

    while nodes:
        min_node = None
        for node in nodes:
            if node in visited:
                if min_node is None:
                    min_node = node
                elif visited[node] < visited[min_node]:
                    min_node = node
        if min_node is None:
            break

        nodes.remove(min_node)
        current_weight = visited[min_node]

        for edge in graph.edges[min_node]:
            try:
                weight = current_weight + graph.distances[(min_node, edge)]
            except:
                continue
            if edge not in visited or weight < visited[edge]:
                visited[edge] = weight
                path[edge] = min_node

    return visited, path


def shortest_path(graph, origin, destination):
    visited, paths = dijkstra(graph, origin)
    full_path = deque()
    _destination = paths[destination]

    while _destination != origin:
        full_path.appendleft(_destination)
        _destination = paths[_destination]

    full_path.appendleft(origin)
    full_path.append(destination)

    return visited[destination], list(full_path)

if __name__ == '__main__':	
	if len(sys.argv) < 5:
		print("\nBad arguments. Usage: \n" + sys.argv[0] + "[CoordinateFilePath]"
		+ "[EdgeFilePath] Latitude Longitude")
		exit(0)
		
	#If extra arguments are given, we do debug mode - don't send path
	debug = False
	if len(sys.argv) == 6:
		debug = True;
		
	#Open the coordinate file and load into the coordinate structure	
	points = []
	with open(sys.argv[COORDFILE_INDEX]) as coordinateFile:
		pointNumber = 0
		for line in coordinateFile:
			lineSplit = line.split()
			points.append([pointNumber, float(lineSplit[0]), float(lineSplit[1])])
			pointNumber += 1
		coordinateFile.close()
		
	graph = Graph()
	
	for node in points:
		graph.add_node(node)
	
	with open(sys.argv[EDGEFILE_INDEX]) as edgeFile:
		for line in edgeFile:
				lineSplit = line.split()
				graph.add_edge(points[int(lineSplit[0])], points[int(lineSplit[1])])
		edgeFile.close()
	
	lat = float(sys.argv[ARGV_LAT_INDEX])
	long = float(sys.argv[ARGV_LONG_INDEX])	
	
	target = [0, lat, long]
	minPointIndex = 0
	minDist = sys.maxsize
	#Find the closest point to the target
	for node in points:
		dist = get_distance(target, node)
		if dist < minDist:
			minPointIndex = node[0]
			minDist = dist
		
	
	
	visited, path = shortest_path(graph, points[0][0], points[minPointIndex][0])
	
	######For getting shortest path without sending packets, use debug
	if debug:
		message = []
		for point in path:
			message.append(str(points[point][0]) + " ")
		print("".join(message))		
		exit(0)
	
	message = []
	for point in path:
		message.append("201 " + str(points[point][LAT_INDEX]) + " " + str(points[point][LONG_INDEX]) + " 260 ")
	print(''.join(message))

	clientSocket = socket(AF_INET, SOCK_STREAM)
	#clientSocket.connect(("192.168.0.24", 11711))
	clientSocket.connect(("cirainbowdrone.ddns.net", 11711))
	clientSocket.send(''.join(message))
	print("Sent!")
	data = clientSocket.recv(256)
	print( "Received data:", data)
	clientSocket.close