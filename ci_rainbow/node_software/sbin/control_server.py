#!/usr/bin/env python3
"""
The control server provides an HTTP REST API for updating configuration files, updating sensor packages, and restarting
the node manager.

The server runs on port 9001 (it's over 9000).
The URLs that may be accessed are:

/update_config
    POST request
    PARAMETERS: commands
    commands is a JSON string containing a list of commands to run, including:
        add, update, remove sections/keys/values of the cirainbow.conf file.
        Additional configuration files may be specified as well.
/update_sensor
    POST request
    PARAMETERS: snapshot
    Downloads a file to /tmp/snapshot.tar.gz and unpackages relative to the CIRAINBOWPATH
/restart_manager
    Touches the .node_running file to change the system timestamp.
"""

import os
import cgi
import json
import urllib
import logging
import configparser
from http.server import HTTPServer, BaseHTTPRequestHandler


# Some path variables to keep track of where things are relatvie to the environment variable CIRAINBOWPATH
cir_path = os.environ.get('CIRAINBOWPATH', None)
log_path = cir_path + '/log/control_server.log'
config_file_path = cir_path + '/conf/cirainbow.conf'
sensor_exec_path = cir_path + '/bin/'


def main():
    """
    Checks the pathing requirements, enables logging, then starts the HTTP server.
    """
    # Check to make sure cir_path has been set!
    if cir_path is None:
        print('Environment variable CIRAINBOWPATH has not been set')
        print('Fix this error by adding the following to your bash profile:')
        print('export CIRAINBOWPATH="/opt/cirainbow/')
        exit()

    # enable to some logging features
    logging.basicConfig(filename=log_path, format='%(asctime)s %(levelname)s %(message)s', level=logging.DEBUG)

    server = HTTPServer(('', 9001), ControlHandler)
    logging.info('Control server started')
    server.serve_forever()


class ControlHandler(BaseHTTPRequestHandler):
    """
    The Control Handler class is a HTTP request handler that receives all the incoming POST requests.
    """

    # handle incoming post requests
    def do_POST(self):
        """
        Handles the incoming POST requests
        :returns: a JSON string containing a list of errors, otherwise a success message.
        """
        error_message = ''

        url = self.path

        if url == '/update_config/':
            logging.info('Recieved an update config command')
            error_message += self.update_config('cirainbow')
        elif url == '/update_sensor/':
            logging.info('Recieved an update sensor command')
            error_message += self.update_sensor()
        elif url == '/restart_manager/':
            error_message += self.restart_manager()

        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()

    # update the watch file to trigger a node manager restart
    @staticmethod
    def restart_manager():
        """
        Updates the .node_running file to trigger a node manager restart.
        """
        logging.info('Received node restart command.')
        f = open(cir_path + 'sbin/.node_running', 'w')
        f.write('1')
        f.close()
        return ''
   
# update configuration file
    def update_config(self, config_file_name):
        """
        Receives a list of commands and updates the configuration file accordingly.

        :param config_file_name: the name of the configuration file to edit
        :return errors: Returns the errors encountered if any
        :rtype: basestring
        """
        errors = ""
        logging.debug("Received config file update request for: " + config_file_name)
        config_handler = ConfigHandler()

        if config_file_name == "cirainbow":
            file_path = cir_path + "/conf/cirainbow.conf"
        else:
            file_path = cir_path + "/conf/" + config_file_name + ".conf"

        length = int(self.headers['Content-Length'])
        post_data = urllib.parse.parse_qs(self.rfile.read(length).decode('utf-8'))
        logging.debug("Data recieved: " + str(post_data))
        logging.debug("Command: " + str(post_data['command'][0]))

        #commands = json.loads(post_data['command'][0])
        for line in str(post_data['command'][0]).split('\n'):
            print(line)
            command = json.loads(line)
            if command['command'] == "add_section":
                errors += config_handler.add_section(file_path, command['section'])
            elif command['command'] == "remove_section":
                errors += config_handler.remove_section(file_path, command['section'])
            elif command['command'] == "edit_section":
                errors += config_handler.edit_section(file_path, command['section'], command['new_name'])
            if command['command'] == "add_key":
                errors += config_handler.add_key(file_path, command['section'], command['key'], command['value'])
            elif command['command'] == "remove_key":
                errors += config_handler.remove_key(file_path, command['section'], command['key'])
            elif command['command'] == "edit_key":
                errors += config_handler.edit_key(file_path, command['section'], command['key'], command['new_name']
                                                  , command['value'])
            else:
                logging.error("Command: " + command['command'] + " not found.")
                errors += "Command: " + command['command'] + " not found."

        return errors

    # add/update a sensor package
    def update_sensor(self):
        """
        Handles the POST request to unpack a sensor package.
        """
        modified_header = self.convert_header()

        ctype, pdict = cgi.parse_header(modified_header)
        pdict['boundary'] = bytes(pdict['boundary'], 'utf-8')

        query = cgi.parse_multipart(self.rfile, pdict)
        snapshot = query.get('snapshot')

        # save file to temporary directory
        with open(cir_path + 'tmp/snapshot.tar.gz', 'bw') as file:
            file.write(snapshot[0])

        # unpack snapshot
        os.system('tar -zxf ' + cir_path + 'tmp/snapshot.tar.gz -C ' + cir_path)

        # remove snapshot
        os.remove(cir_path + 'tmp/snapshot.tar.gz')

        return ''

    def convert_header(self):
        """
        Required due to a bug in the cgi-parse package. Needed to convert the headers to a format that it can be
        read in.
        """
        modified_header = ''
        for c in str(self.headers):
            if c == ':':
                modified_header += '='
            elif '\n' in c:
                modified_header += ';'
            else:
                modified_header += c
        return modified_header


class ConfigHandler:
    """
    Contains static methods to perform the following operations:
    add/remove/edit a section in a configuration file
    add/remove/edit a key/value pair in a configuration file
    """

    @staticmethod
    def add_section(file_name, section):
        """
        Adds a section to a configuration file if it does not already exist.

        :param file_name: the name of the configuration file to edit
        :param section: adds section if it does not already exist.
        :return:
        """
        config = configparser.ConfigParser()
        config.read(file_name)

        if not config.has_section(section):
            config.add_section(section)
            with open(file_name, 'w') as configFile:
                config.write(configFile)
            logging.info("Added section: " + section + " to " + file_name)
        else:
            logging.error("Cannot make section: " + section + ", already exists.")
            return "Cannot make section: " + section + ", already exists.\n"

        return ""

    @staticmethod
    def remove_section(file_name, section):
        """
        Removes a section from a configuration file if it exists.

        :param file_name: the name of the configuration file to edit
        :param section: removes section from the configuration file if it exists
        :return:
        """
        config = configparser.ConfigParser()
        config.read(file_name)

        if config.has_section(section):
            config.remove_section(section)
            with open(file_name, 'w') as configFile:
                config.write(configFile)
            logging.info("Removed section: " + section + " to " + file_name)
        else:
            logging.error("Cannot remove section: " + section + ", does not exists.")
            return "Cannot remove section: " + section + ", does not exists.\n"

        return ""

    @staticmethod
    def edit_section(file_name, section, new_name):
        """
        Renames a section in the configuration file if it exists.

        :param file_name: The name of the configuration file to edit.
        :param section: The name of the section to change.
        :param new_name: The name the old section will be renamed to.
        :return:
        """
        config = configparser.ConfigParser()
        config.read(file_name)

        if config.has_section(section):
            if config.has_section(new_name):
                logging.error("Cannot edit section: " + section + " to " + new_name + ", newName already exists.")
                return "Cannot edit section: " + section + " to " + new_name + ", newName already exists.\n"
            else:
                config.add_section(new_name)
                keys = config.options(section)
                for key in keys:
                    value = config.get(section, key)
                    config.set(new_name, key, value)
                config.remove_section(section)
                with open(file_name, 'w') as configFile:
                    config.write(configFile)
                logging.info("Changed section: " + section + " to: " + new_name)
        else:
            logging.error("Cannot edit section: " + section + ", does not exists.")
            return "Cannot edit section: " + section + ", does not exists.\n"

        return ""

    @staticmethod
    def add_key(file_name, section, key, value):
        """
        Adds a key/value pair to a section, if the section exists and if the key does not already exist.

        :param file_name: The name of the configuration file to edit.
        :param section: The name of the section to add the key/value pair to.
        :param key: The name of the key to add to the section.
        :param value: The value to associate with the key.
        :return:
        """
        config = configparser.ConfigParser()
        config.read(file_name)

        if config.has_section(section):
            if config.has_option(section, key):
                logging.error("Key " + key + " already exists.")
                return "Cannot add key, " + key + ", already exists."
            else:
                config.set(section, key, value)
                with open(file_name, 'w') as configFile:
                    config.write(configFile)
                logging.info("Added key,value: " + key + ", " + value + " to section: " + section)
        else:
            logging.error("Cannot add key: " + key + ", section: " + section + ", does not exists.")
            return "Cannot add key: " + key + ", section: " + section + ", does not exists.\n"

        return ""

    @staticmethod
    def remove_key(file_name, section, key):
        """
        Removes a key from a section if it exists.

        :param file_name: The name of the configuration file to edit.
        :param section: The name of the section that the key is in.
        :param key: The name of the key to be removed.
        :return:
        """
        config = configparser.ConfigParser()
        config.read(file_name)

        if config.has_section(section):
            if not config.has_option(key):
                logging.error("Key " + key + " does not exists.")
                return "Cannot remove key, " + key + ", does notalready exists."
            else:
                config.remove_option(section, key)
                with open(file_name, 'w') as configFile:
                    config.write(configFile)
        else:
            logging.error("Cannot remove key: " + key + ", section: " + section + " does not exists.")
            return "Cannot remove key: " + key + ", section: " + section + " does not exists.\n"

        return ""

    @staticmethod
    def edit_key(file_name, section, key, new_name, value):
        """
        Updates a keys name or value or both.

        :param file_name: The name of the configuration file to edit.
        :param section: The section that contains the key/value pair.
        :param key: The name of the key.
        :param new_name: optional, if a new name for the key is desired.
        :param value: optional, if a new value for the key is desired.
        :return:
        """
        config = configparser.ConfigParser()
        config.read(file_name)

        if config.has_section(section):
            if config.has_option(new_name):
                logging.error("Cannot edit key: " + key + " to " + new_name + ", key already exists.")
                return "Cannot edit key: " + key + " to " + new_name + ", key already exists.\n"
            else:
                config.set(section, new_name, value)
                if value == "":
                    config.set(section, new_name, config.get(section, key))
                config.remove_option(section, key)
                with open(file_name, 'w') as configFile:
                    config.write(configFile)
                logging.info("Changed key: " + section + " to: " + new_name)
        else:
            logging.error("Cannot edit key: " + key + "section: " + section + ", does not exists.")
            return "Cannot edit key: " + key + "section: " + section + ", does not exists.\n"

        return ""


main()
