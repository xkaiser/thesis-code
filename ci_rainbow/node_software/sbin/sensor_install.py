#!/usr/bin/env python3
"""
The sensor install script is used during the initial setup of the sensor node. A user may install a sensor via ID num.
"""

import os

cir_path = os.environ.get('CIRAINBOWPATH', None)
config_file_path = cir_path + '/conf/cirainbow.conf'
sensor_exec_path = cir_path + '/bin/'

