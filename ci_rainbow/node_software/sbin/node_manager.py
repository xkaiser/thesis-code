#!/usr/bin/env python3
"""
The Node Manager is responsible for starting the sensor scripts on a timed interval and queueing/sending the data on a
timed interval.
"""

import os
import sys
import time
import shutil
import logging
import requests
import configparser
from threading import Timer
from os.path import getmtime


# Some path variables to keep track of where things are relative to the environment variable CIRAINBOWPATH
cir_path = os.environ.get('CIRAINBOWPATH', '')
node_log_path = cir_path + '/log/node_manager.log'
config_file_path = cir_path + '/conf/cirainbow.conf'
sensor_exec_path = cir_path + '/bin/'
data_path = cir_path + '/data/'
data_queue_path = data_path + '/queue/'
data_archive_path = data_path + '/archive/'

# A global variable to maintain the current state of the Node Manager
state = 0
timers = {}

# Configuration information read from /conf/cirainbow.conf
node_info = dict([('node_id', ''), ('ip_address', '')])
server_info = dict([('data_servlet', ''), ('server', ''), ('send_interval', '')])
sensors = []


# Data structure for holding sensor information
class Sensor:
    pass


def main():
    """
    Runs a continuous state machine loop and performs the following:
    State 0: Read the configuration files and report any errors. Goto state 1.
    State 1: Start the sensor threads and send data threads on timers. Goto state 2
    State 2: Watch the .node_running file for file system changes, if changes are found, goto state 3.
    State 3: Attempt to gracefully restart the Node Manager.
    """
    global state

    # Check to make sure cir_path has been set!
    if cir_path is '':
        print('Environment variable CIRAINBOWPATH has not been set')
        print('Have you run the setup script yet?')
        exit()

    # Enable to some logging features
    logging.basicConfig(filename=node_log_path, format='%(asctime)s %(levelname)s %(message)s', level=logging.DEBUG)

    # Create file to watch to restart node
    if os.path.exists('.node_running'):
        os.remove('.node_running')
    open('.node_running', 'a').close()
    mtime = getmtime('.node_running')

    # Start the state machine
    while True:
        if state == 0:
            logging.info('Node manager has been started.')
            logging.info('Reading the configuration file.')
            if read_configuration() is False:
                logging.error('Failed to read configuration file. Exiting...')
                exit()
            state = 1
        elif state == 1:
            logging.info('Starting the attached sensors.')
            logging.info('Starting the send data intervals.')
            start_sensor_threads()
            start_data_thread()
            state = 2
        elif state == 2:
            if getmtime('.node_running') != mtime:
                state = 3
            time.sleep(0.2)
        elif state == 3:
            logging.warning('Received a restart command.')
            # Cancel currently running sensor threads
            logging.info('Cancelling sensor threads...')
            for key in timers.keys():
                timers[key].cancel()
            # Restart Node Manager
            logging.info('Restarting node manager...')
            os.remove('.node_running')
            os.execv(__file__, sys.argv)


def read_configuration():
    """
    Reads the /conf/cirainbow.conf file using the configparser library. Three sections of the conf file are read:
    'node_info' is read into the node_info dictionary
    'server_info' is read into the server_info dictionary
    'sensors' is read into a sensors class.

    :returns: True if configuration read successfully, False if issues encountered.
    :rtype: Boolean
    """
    global server_info, sensors, node_info

    config = configparser.ConfigParser()
    config.read(config_file_path)

    # Read in the Node Information
    for key in node_info.keys():
        if config.has_option('node_info', key):
            node_info[key] = config.get('node_info', key)
            logging.info('Node information key: ' + key + ' -> ' + node_info[key])
        else:
            logging.error("Server information key: " + key + " is not the configuration file")
            return False

    # read in the webapp server information
    for key in server_info.keys():
        if config.has_option('server_info', key):
            server_info[key] = config.get('server_info', key)
            logging.info('Server information key: ' + key + ' -> ' + server_info[key])
        else:
            logging.error("Server information key: " + key + " is not the configuration file")
            return False

    # read in the currently enabled sensors
    sensor_conf = config.options('sensors')
    for item in sensor_conf:
        sensor = Sensor()

        sensor_info = config.get('sensors', item).split(',')

        sensor.name = item
        sensor.id = sensor_info[0]
        sensor.exec = sensor_info[1]
        sensor.interval = sensor_info[2]
        logging.info('Read in a sensor: ' + sensor.name + ', ' + sensor.id + ', ' + sensor.exec +
                     ', ' + sensor.interval)

        sensors.append(sensor)

    return True


def start_sensor_threads():
    """
    Starts each sensor contained in the sensors list on a timed interval. The interval is read from the configuration
     file. The run_sensor() function is called with the sensor as an argument. Each timer is saved to a dictionary
     in order to cancel the timers in the event of a restart command.
    """
    for sensor in sensors:
        timers[sensor.name] = Timer(int(sensor.interval), run_sensor, [sensor]).start()
        logging.info('Running ' + sensor.name + ' every ' + sensor.interval)


def run_sensor(sensor):
    """
    The sensor is executed and a new timer is created. The new timer replaces the old timer in the timers dictionary.

    :param sensor: The sensor class representing the current sensor.
    :type sensor: Sensor class.
    """
    os.system( sensor_exec_path + sensor.exec )
    timers[sensor.name] = Timer(int(sensor.interval), run_sensor, [sensor]).start()


def start_data_thread():
    """
    Begins the data sending thread on an interval specified in the conf/cirainbow.conf file.
    """
    Timer(int(server_info['send_interval']), queue_and_send_data).start()


def queue_and_send_data():
    """
    Called from a send timer, this function queues the data, sends it, and starts another send data thread timer.
    """
    queue_data()
    send_data()
    start_data_thread()


def queue_data():
    """
    Searches for all the data files in /data ending with '.dat'. These files are renamed and placed in /data/queue
     meaning they are ready to be sent to the server.

    The renaming scheme is to append a (x) before the file extension of each .dat file, where x is [1,2,3...].

    note: It is important that the data file has the same name as the sensor in the configuration file. This way the
    sensor may be reverse identified by the name.
    """
    data_files = os.listdir( data_path)

    # check all the files ending with .dat in the data directory
    for data_file in data_files:
        if data_file.endswith('.dat'):
            # rename them and queue them, ex: temp.dat => temp(1).dat and so on
            i = 1
            destination = data_queue_path + data_file[:-4] + '(' + str(i) + ')' + data_file[-4:]
            while os.path.exists(destination ):
                i += 1
                destination = data_queue_path + data_file[:-4] + '(' + str(i) + ')' + data_file[-4:]

            # move and rename the file
            shutil.move(data_path + data_file, destination)


def send_data():
    """
    Sends all the data in the /data/queue directory.
    """
    data_files = os.listdir( data_queue_path )
    sensor_files = {}
    data = {}

    if data_files:
        # look at all the data files
        for data_file in data_files:
            sensor_name = data_file.split('/')[-1].split('(')
            sensor_id = get_sensor_by_name(sensor_name[0]).id

            # save the filename and associate with an id
            # we need to do this so we can move the files to the archived folder
            if sensor_id not in sensor_files:
                sensor_files[sensor_id] = []
            sensor_files[sensor_id].append(data_queue_path + data_file)

            # read all the json data associated with a sensor
            fp = open(data_queue_path + data_file, 'r')
            if sensor_id not in data:
                data[sensor_id] = ''
            data[sensor_id] += fp.read()
            fp.close()

        # send all the data associated with a sensor
        for sensor_id in data:
            server_url = server_info['server'] + server_info['data_servlet'] + str(node_info['node_id']) + '/' + str(sensor_id) + '/'
            r = requests.post(server_url, data={'data': data[sensor_id]})
            response = r.text

            # if success, move to the archived folder
            if check_server_response(response):
                for sensor_file in sensor_files[sensor_id]:
                    shutil.move(sensor_file, data_archive_path + sensor_file.split('/')[-1])
            else:
                # TODO this is a stub.
                logging.error("Failed to send data.")


def check_server_response(response):
    # TODO this is a stub.
    return True


def get_sensor_by_name(name):
    for sensor in sensors:
        if sensor.name == name:
            return sensor
    return None


# let's get started
main()
