#!/usr/bin/env python3

import os
import time
import random
import requests
import calendar
import configparser

# get the full path of the CI Rainbow system
cir_path = os.environ.get('CIRAINBOWPATH', '')
config_file_path = cir_path + '/conf/cirainbow.conf'

server_url = "http://127.0.0.1:8000/cirainbow"
node_id = 5

if cir_path is None:
    print("Fatal error - CIRAINBOWPATH environment variable not set")
    exit()



def poll_sensor():
    temp = 95.0
    deci = random.randrange(0,10)
    temp += deci
    return temp

config = configparser.ConfigParser()
config.read(config_file_path)

event_id = None
if config.has_section('temp_events'):
	for key in config.options('temp_events'):
		event_info = config.get('temp_events', key)
		event_id = key
		trigger_id = event_info[0]
		parameter_value = int(event_info[1])



current_temp = poll_sensor()
if event_id:
	if trigger_id == 3: # above X trigger
		if current_temp > parameter_value:
			r = requests.post(server_url + "/event/" + str(event_id) + "/" + str(trigger_id) + "/" + str(node_id)
		

data = {"date": calendar.timegm(time.time()), "data": {"type": "F", "value": current_temp}}

with open(cir_path + "/data/test.dat", "a") as fp:
    fp.write(str(data))
    fp.write('\n')
