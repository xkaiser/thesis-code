#@IKenshinI
#!/usr/bin/env python
#import socket module
#Sits on ubuntu laptop and receives a path
#This will send the drone.
#Place this at "cd  paparazzi/sw/lib/python"
from __future__ import print_function
from socket import *
from time import sleep

import sys
from os import path, getenv

# if PAPARAZZI_SRC not set, then assume the tree containing this
# file is a reasonable substitute
PPRZ_SRC = getenv("PAPARAZZI_SRC", path.normpath(path.join(path.dirname(path.abspath(__file__)), '../../../../')))
sys.path.append(PPRZ_SRC + "/sw/lib/python")

from ivy_msg_interface import IvyMessagesInterface
from pprz_msg.message import PprzMessage

class WaypointMover(object):
    def __init__(self, verbose=False):
        self.verbose = verbose
        self._interface = IvyMessagesInterface(self.message_recv)

    def message_recv(self, ac_id, msg):
        if self.verbose:
            print("Got msg %s" % msg.name)

    def shutdown(self):
        print("Shutting down ivy interface...")
        self._interface.shutdown()

    def __del__(self):
        self.shutdown()

    def move_waypoint(self, ac_id, wp_id, lat, lon, alt):
        msg = PprzMessage("ground", "MOVE_WAYPOINT")
        msg['ac_id'] = ac_id
        msg['wp_id'] = wp_id
        msg['lat'] = lat
        msg['long'] = lon
        msg['alt'] = alt
        print("Sending message: %s" % msg)
        self._interface.send(msg)

    def jump_block(self, ac_id, block_id):
        msg = PprzMessage("ground", "JUMP_TO_BLOCK")
        msg['ac_id'] = ac_id
        msg['block_id'] = block_id
        print("Sending message: %s" % msg)
        self._interface.send(msg)

if __name__ == '__main__':
        waypointIDStart = 4
	commandBlockID = 6
    	serverSocket = socket(AF_INET, SOCK_STREAM)
	serverSocket.bind(('0.0.0.0',11711))
	serverSocket.listen(1)
	wm = WaypointMover()
	while True:
		print('Ready to serve')
		connectionSocket, addr = serverSocket.accept()
		try:
			message = connectionSocket.recv(2048)
			print(message)
			messageSplit = message.split()
			numWaypointsRecv = len(messageSplit)/4
			for i in xrange(0, numWaypointsRecv):
				ind = i * 4
				wm.move_waypoint(ac_id=messageSplit[ind], \
					wp_id= waypointIDStart + i, \
					lat=messageSplit[ind + 1], \
					lon=messageSplit[ind + 2], \
					alt=messageSplit[ind + 3])
				sleep(.5)
			lastInd = (numWaypointsRecv - 1) * 4
			for i in xrange(numWaypointsRecv, 64):
				wm.move_waypoint(ac_id=messageSplit[lastInd], \
					wp_id= waypointIDStart + i, \
					lat=messageSplit[lastInd + 1], \
					lon=messageSplit[lastInd + 2], \
					alt=messageSplit[lastInd + 3])
				sleep(0.5)
			wm.jump_block(ac_id=messageSplit[0], \
				block_id=commandBlockID);
			connectionSocket.send("Sent")
		except IOError:
			serverSocket.close()
			break
	serverSocket.close()
    
    
